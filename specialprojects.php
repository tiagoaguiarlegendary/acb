<?php
/*
Template Name: Projetos Especiais
*/

get_header();
?>

<div class="container birkita_archive-content-wrap content-sb-section clear-fix" id="projetosEspeciais">
  <div class="birkita_header">
          <div class="main-title">
              <h3><?php the_title(); ?></h3>
          </div>
          <div class="content mt-20">
              <p><?php echo get_post_field('post_content', $post->ID); ?></p>
          </div>

      </div>
    <?php
    $projects = array(
    'post_status'    =>     'publish',
    'posts_per_page' =>     -1,
    'nopaging'       =>     true,
    'post_type'      =>     'projetos'
    );
    $projects = new WP_Query($projects);

    if($projects->have_posts()):
          while ($projects->have_posts()): $projects->the_post(); ?>

        <div class="row projects-column">
          <div class="col-md-8">
            <h2 style="text-transform: uppercase;"><?php the_title();?></h2>
            <p><?php the_content(); ?></p>
          </div>
          <div class="col-md-4">
            <div class="owl-carousel owl-theme">
              <?php if(have_rows('images_repeater')):  while(have_rows('images_repeater')): the_row();?>

                  <div class="item"><img src="<?php echo get_sub_field('images'); ?>" alt="<?php the_title(); ?>"></div>
              <?php endwhile;
              wp_reset_postdata();
             endif; ?>
            </div>
          </div>
        </div>
      <?php endwhile;
    endif; ?>

</div>
<?php
get_footer();

?>
<script>
$('.owl-carousel').owlCarousel({
  loop:true,
  margin:10,
  nav: false,
  responsive:{
      0:{
          items:1
      },
      600:{
          items:1
      },
      1000:{
          items:1
      }
  }
});
</script>
