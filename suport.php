<?php
/*
Template Name: Apoio ao Associado
*/
?>
<?php get_header();?>

      <div class="article-content">
        <div class="container-fluid">
          <div class="birkita_header">
              <div class="main-title" style="margin-top: 35px;">
                  <h3>
                      <?php the_title();?>
                  </h3>
              </div>
          </div>
        </div>
        <div class="container" id="service-heading">
            <div class="row">
                <div class="col-12">
                    <p><?php echo get_post_field('post_content', $post->ID); ?></p>
                </div>
            </div>
        </div>
        <div class="container" id="service-content">
          <div class="row service-row">
              <?php
              if( have_rows('repeater_advantages') ):
                  while ( have_rows('repeater_advantages') ) : the_row(); ?>

                      <?php
                          $vantagem = get_sub_field('vantagem');
                          $servico = get_sub_field('servico');
                       ?>
                        <div class="col-12 advantage-container">
                          <h3><?php echo $vantagem ?></h3>
                          <div class="servico-hidden">
                            <p><?php echo $servico; ?></p>
                          </div>
                        </div>

              <?php endwhile;
              endif;

              ?>
        </div>
      </div>


      </div>

        <?php

          if ( is_active_sidebar( 'fullwidth_section_bottom' )):?>
          		<div class="fullwidth-section bottom">
                      <?php dynamic_sidebar('fullwidth_section_bottom'); ?>
          		</div>
          <?php
          endif;




 get_footer();?>
