<?php
/*
Template Name: Associado Num Minuto
*/
?>
<?php get_header();?>

      <div class="article-content">
        <div class="container-fluid">
          <div class="birkita_header">
              <div class="main-title" style="margin-top: 35px;">
                  <h3>
                      <?php the_title();?>
                  </h3>
              </div>
          </div>

        </div>
        <div class="container">
            <div class="row">
                <div class="col-12">
                    <p><?php echo get_post_field('post_content', $post->ID); ?></p>
                </div>
            </div>
        </div>
        <div class="container">
          <div class="row service-row">
            <?php echo do_shortcode('[contact-form-7 id="11" title="Contact form 1"]'); ?>
        </div>
        <div class="row" style="margin-top: 35px;">
          <div class="col-md-12">
              <p style="text-align:center;">Ainda não se decidiu? Veja todas as vantagens em se tornar associado <a href="<?php echo get_permalink(get_page_by_title('Vantagens Associados')); ?>">aqui</a></p>
          </div>

        </div>
      </div>


      </div>


<?php get_footer();?>

<script>
  $(document).ready(function(){
    //Function to handle NIF fields, first parameter is the input field, second the maxLenght
    $('input#nif').keypress(function() {
      handleNif($('#nif'));
    });

  });
</script>
