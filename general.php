<?php
/*
Template Name: Serviços
*/
?>
<?php get_header();?>

      <div class="article-content">
        <div class="container-fluid">
          <div class="birkita_header">
              <div class="main-title" style="margin-top: 35px;">
                  <h3>
                      <?php the_title();?>
                  </h3>
              </div>
          </div>
          <img src="<?php echo get_field('main_image');?>" alt="Serviços ACB">
        </div>
        <div class="container" id="service-heading">
            <div class="row">
                <div class="col-12">
                    <p><?php echo get_post_field('post_content', $post->ID); ?></p>
                </div>
            </div>
        </div>
        <div class="container" id="service-content">
          <div class="row service-row">
              <?php
              if( have_rows('acb_services_repeater') ):
                  while ( have_rows('acb_services_repeater') ) : the_row(); ?>

                      <?php
                          $link = get_sub_field('page_link');
                          $desc = get_sub_field('breve_descricao');
                       ?>
                        <div class="col-md-4 col-xs-12" style="text-align:center;">
                          <img src="<?php echo get_sub_field('icone'); ?>" alt="<?php echo get_sub_field('service_title'); ?>" class="icon-img">
                            <div class="birkita_header"><?php echo get_sub_field('service_title'); ?></div>
                            <p><?php echo $desc; ?></p>
                            <a class="next" href="<?php echo $link; ?>" style="top: 0;"> Saber Mais <i class="fa fa-caret-right" style="font-style:normal;"></i></a>
                        </div>

              <?php endwhile;
              endif;

              ?>
        </div>
      </div>


      </div>

        <?php

          if ( is_active_sidebar( 'fullwidth_section_bottom' )):?>
          		<div class="fullwidth-section bottom">
                      <?php dynamic_sidebar('fullwidth_section_bottom'); ?>
          		</div>
          <?php
          endif;

 get_footer();?>
